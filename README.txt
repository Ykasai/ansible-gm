Ansible実行方法

①ansibleのインストール
　$ sudo yum install ansible -y

②hostsにEC2インスタンスのIPを追加する
　$ vi /etc/ansible/hosts

③EC2との接続チェック
　$ ansible -i /etc/ansible/hosts --private-key=<秘密鍵のパス> -u ec2-user -m ping <EC2インスタンスのIP>
  下記のように表示されたら成功
  <EC2インスタンスのIP> | success >> {
    "changed": false,
    "ping": "pong"
　}
　下記のエラーが出る場合、空の「known_hosts」ファイルを作成する
　No such file or directory: '/home/vagrant/.ssh/known_hosts'

④playbook.ymlの書き換え
　$ vi playbook.yml
  １行目にipアドレスを記入
   - hosts : xx.xx.xx.xx 

⑤playbookの実行
　$ ansible-playbook --private-key=<秘密鍵のパス> ./playbook.yml